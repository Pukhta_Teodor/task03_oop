package com.pukhta.view;

@FunctionalInterface
public interface Printable {

    void print();
}
