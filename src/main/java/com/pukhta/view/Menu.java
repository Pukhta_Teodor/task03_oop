package com.pukhta.view;

import com.pukhta.controller.ControllerImpl;
import com.pukhta.controller.Controller;

import java.util.*;

/**
 * Class realises menu of the program
 */
public class Menu {
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public Menu() {

        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - print all devices");
        menu.put("2", "  2 - calculate power usage");
        menu.put("3", "  3 - sort devices by power");
        menu.put("4", "  4 - search device by parametres");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
    }

    /**
     * realise of typing button 1
     */
    private void pressButton1() {
        System.out.println(controller.getDeviceList());
    }

    /**
     * realise of typing button 2
     */
    private void pressButton2() {
        System.out.println("Power usage - " + controller.calculatePowerUsage());
    }

    /**
     * realise of typing button 3
     */
    private void pressButton3() {
        controller.sortDevicesByPower();
        System.out.print("Devices were sorted by power");
    }

    /**
     * realise of typing button 4
     */
    private void pressButton4() {
        System.out.println("Input power interval:");
        double pow1 = Double.parseDouble(input.nextLine());
        double pow2 = Double.parseDouble(input.nextLine());
        controller.searchDevice(pow1, pow2);

    }

    /**
     * shows list of menu parametres
     */
    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    /**
     * shows menu
     */
    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
