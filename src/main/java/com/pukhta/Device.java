package com.pukhta;

/**
 * class descrives  Device in house
 */
public class Device {

    /**
     * type of device
     */
    DeviceType deviceType;

    /**
     * power usage of device
     */
    private double power;

    /**
     * is device connected to power grid
     */
    private boolean connectStatus;

    /**
     * price of device
     */
    private int price;
    /**
     * constructor of class
     */
    public Device(DeviceType deviceType, double power,
                  boolean connectStatus, int price) {
        this.deviceType = deviceType;
        this.power = power;
        this.connectStatus = connectStatus;
        this.price = price;
    }


    public double getPower() {
        return power;
    }

    public void setPower(double power) {
        this.power = power;
    }

    public boolean isConnectStatus() {
        return connectStatus;
    }

    @Override
    public String toString() {
        return "Device{" +
                "deviceType=" + deviceType +
                ", power=" + power +
                ", connectStatus=" + connectStatus +
                ", price=" + price +
                '}';
    }
}
