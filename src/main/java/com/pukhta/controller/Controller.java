package com.pukhta.controller;

import com.pukhta.Device;

import java.util.List;

/**
 * Controller interface
 */
public interface Controller {
    /**
     * returns list of devices
     */
    List<Device> getDeviceList();

    /**
     * calculationg power usage of connected devices
     */
    double calculatePowerUsage();

    /**
     * sort devices by power usage
     */
    void sortDevicesByPower();

    /**
     * search devices in range
     */
    void searchDevice(double power1, double power2);
}
