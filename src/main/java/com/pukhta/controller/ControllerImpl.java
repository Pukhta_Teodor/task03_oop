package com.pukhta.controller;

import java.util.List;

import com.pukhta.Device;
import com.pukhta.model.*;

/**
 * implementation of Controller
 */
public class ControllerImpl implements Controller {
    private Model model;

    public ControllerImpl() {
        model = new BusinessLogic();
    }

    /**
     * returns list of devices
     */
    @Override
    public List<Device> getDeviceList() {
        return model.getDeviceList();
    }

    /**
     * calculationg power usage of connected devices
     */
    @Override
    public double calculatePowerUsage() {
        return model.calculatePowerUsage();
    }

    /**
     * sort devices by power usage
     */
    @Override
    public void sortDevicesByPower() {
        model.sortDevicesByPower();
    }

    /**
     * search devices in range
     */
    @Override
    public void searchDevice(double power1, double power2) {
        model.searchDevice(power1, power2);
    }

}
