package com.pukhta.model;

import com.pukhta.Device;
import com.pukhta.DeviceType;

import java.util.*;

/**
 * Domain class
 */
public class Domain {

    private List<Device> deviceList;

    public Domain() {
        deviceList = new LinkedList<>();
        generateNumberList();
    }

    private void generateNumberList() {
        deviceList.add(new Device(DeviceType.FRIDGE, 50.0, true, 2000));
        deviceList.add(new Device(DeviceType.LAMP, 4.5, true, 30));
        deviceList.add(new Device(DeviceType.LAMP, 3.0, false, 30));
        deviceList.add(new Device(DeviceType.CHARGER, 2.0, false, 100));
        deviceList.add(new Device(DeviceType.VACUUM, 15.0, false, 600));
        deviceList.add(new Device(DeviceType.COMPUTER, 20.0, true, 20000));
        deviceList.add(new Device(DeviceType.LAMP, 4.5, true, 40));
        deviceList.add(new Device(DeviceType.TV, 10.3, false, 5000));
        deviceList.add(new Device(DeviceType.TV, 6.7, true, 1000));
        deviceList.add(new Device(DeviceType.CONDITIONER, 5.0, true, 800));
        deviceList.add(new Device(DeviceType.CHARGER, 13.2, false, 14));
    }

    /**
     * returns list of devices
     */
    public List<Device> getDeviceList() {
        return deviceList;
    }

    /**
     * calculationg power usage of connected devices
     */
    public double calculatePowerUsage() {
        double sum = 0;
        for (Device dev : deviceList) {
            if (dev.isConnectStatus()) {
                sum += dev.getPower();
            }
        }
        return sum;
    }

    /**
     * sort devices by power usage
     */
    public void sortDevicesByPower() {
        deviceList.sort(Comparator.comparing(Device::getPower));
    }

    /**
     * search devices in range
     */
    public void searchDevice(double power1, double power2) {
        int i = 0;
        for (Device dev : deviceList) {
            if (dev.isConnectStatus()) {
                if (power1 <= dev.getPower() && dev.getPower() <= power2) {
                    System.out.println(dev.toString());
                    i++;
                }
            }
        }
        if (i == 0) {
            System.out.println("There are no devices in such inverval");
        }
    }
}

