package com.pukhta.model;

import com.pukhta.Device;

import java.util.List;

/**
 * business logic class
 */
public class BusinessLogic implements Model {

    private Domain domain;

    public BusinessLogic() {
        domain = new Domain();
    }

    /**
     * returns list of devices
     */
    @Override
    public List<Device> getDeviceList() {
        return domain.getDeviceList();
    }

    /**
     * sort devices by power usage
     */
    @Override
    public void sortDevicesByPower() {
        domain.sortDevicesByPower();
    }

    /**
     * calculationg power usage of connected devices
     */
    @Override
    public double calculatePowerUsage() {
        return domain.calculatePowerUsage();
    }

    /**
     * search devices in range
     */
    @Override
    public void searchDevice(double power1, double power2) {
        domain.searchDevice(power1, power2);
    }

}

