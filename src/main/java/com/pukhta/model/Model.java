package com.pukhta.model;

import com.pukhta.Device;

import java.util.List;

/**
 * model of the program
 */
public interface Model {
    /**
     * returns list of devices
     */
    List<Device> getDeviceList();

    /**
     * search devices in range
     */
    void searchDevice(double power1, double power2);

    /**
     * sort devices by power usage
     */
    void sortDevicesByPower();

    /**
     * calculationg power usage of connected devices
     */
    double calculatePowerUsage();

}
