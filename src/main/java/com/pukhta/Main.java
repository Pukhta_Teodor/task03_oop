package com.pukhta;

import com.pukhta.view.Menu;

/**
 * Main class of the program
 */
public class Main {
    /**
     * start of the program
     */
    public static void main(final String[] args) {
        Menu menu = new Menu();
        menu.show();
    }
}
