package com.pukhta;

/**
 * list of divices' types
 */
public enum DeviceType {
    FRIDGE, VACUUM, CHARGER, LAMP, COMPUTER, TV, CONDITIONER
}

